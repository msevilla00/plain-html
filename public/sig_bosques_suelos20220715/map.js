// configuración general del mapa: zoom y limites
var map = L.map('map', {
    zoomControl:true, maxZoom:18, minZoom:8
}).fitBounds([[39.880768776603084,-2.971624732002457],[43.106885910087,1.4523986808284945]]);

var hash = new L.Hash(map);

// atribución (créditos) generales del mapa
map.attributionControl.setPrefix('Mapa creado desde <a href="https://qgis.org">QGIS</a> con plugin <a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a> usando <a href="https://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> por M. Sevilla-Callejo CC By-SA 20220714');

var autolinker = new Autolinker({truncate: {length: 30, location: 'smart'}});

var measureControl = new L.Control.Measure({
    position: 'topleft',
    primaryLengthUnit: 'meters',
    secondaryLengthUnit: 'kilometers',
    primaryAreaUnit: 'sqmeters',
    secondaryAreaUnit: 'hectares'
});
measureControl.addTo(map);
document.getElementsByClassName('leaflet-control-measure-toggle')[0]
.innerHTML = '';
document.getElementsByClassName('leaflet-control-measure-toggle')[0]
.className += ' fas fa-ruler';
var bounds_group = new L.featureGroup([]);
function setBounds() {
    map.setMaxBounds(map.getBounds());
}
map.createPane('pane_OpenStreetMap_0');
map.getPane('pane_OpenStreetMap_0').style.zIndex = 400;
var layer_OpenStreetMap_0 = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    pane: 'pane_OpenStreetMap_0',
    opacity: 1.0,
    attribution: '© Colaboradores OpenStreetMap',
    minZoom: 8,
    maxZoom: 20,
    minNativeZoom: 0,
    maxNativeZoom: 19
});
layer_OpenStreetMap_0;
map.createPane('pane_MapatopogrficoIGN_1');
map.getPane('pane_MapatopogrficoIGN_1').style.zIndex = 401;
var layer_MapatopogrficoIGN_1 = L.tileLayer('http://www.ign.es/wmts/mapa-raster?request=GetTile&service=WMTS&VERSION=1.0.0&Layer=MTN&Style=default&Format=image/png&TileMatrixSet=GoogleMapsCompatible&TileMatrix={z}&TileRow={y}&TileCol={x}', {
    pane: 'pane_MapatopogrficoIGN_1',
    opacity: 1.0,
    attribution: 'Mapa topográfico (IGN)',
    minZoom: 8,
    maxZoom: 20,
    minNativeZoom: 0,
    maxNativeZoom: 18
});
layer_MapatopogrficoIGN_1;
map.createPane('pane_OrtoimagenPNOAIGN_2');
map.getPane('pane_OrtoimagenPNOAIGN_2').style.zIndex = 402;
var layer_OrtoimagenPNOAIGN_2 = L.tileLayer('https://www.ign.es/wmts/pnoa-ma?request=GetTile&service=WMTS&VERSION=1.0.0&Layer=OI.OrthoimageCoverage&Style=default&Format=image/png&TileMatrixSet=GoogleMapsCompatible&TileMatrix={z}&TileRow={y}&TileCol={x}', {
    pane: 'pane_OrtoimagenPNOAIGN_2',
    opacity: 1.0,
    attribution: 'Ortoimagen PNOA (IGN)',
    minZoom: 8,
    maxZoom: 20,
    minNativeZoom: 1,
    maxNativeZoom: 20
});
layer_OrtoimagenPNOAIGN_2;
map.createPane('pane_BaseOSMDarkCARTO_3');
map.getPane('pane_BaseOSMDarkCARTO_3').style.zIndex = 403;
var layer_BaseOSMDarkCARTO_3 = L.tileLayer('http://basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
    pane: 'pane_BaseOSMDarkCARTO_3',
    opacity: 1.0,
    attribution: '© CARTO Dark Matter (datos OSM)',
    minZoom: 8,
    maxZoom: 20,
    minNativeZoom: 0,
    maxNativeZoom: 20
});
layer_BaseOSMDarkCARTO_3;
map.addLayer(layer_BaseOSMDarkCARTO_3);

function pop_eu_soil_db_clipped_aragon__4(feature, layer) {
    var popupContent = '<table>\
            <tr>\
            <th scope="row">nombre</th>\
                <td>' + (feature.properties['nombre'] !== null ? autolinker.link(feature.properties['nombre'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">WRB Cod.</th>\
                <td>' + (feature.properties['smu_sgdbe_WRBFU'] !== null ? autolinker.link(feature.properties['smu_sgdbe_WRBFU'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">FAO90 Cod.</th>\
                <td>' + (feature.properties['smu_sgdbe_FAO90FU'] !== null ? autolinker.link(feature.properties['smu_sgdbe_FAO90FU'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">TXSRFDO</th>\
                <td>' + (feature.properties['smu_sgdbe_TXSRFDO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_TXSRFDO'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">SLOPESE</th>\
                <td>' + (feature.properties['smu_sgdbe_SLOPESE'] !== null ? autolinker.link(feature.properties['smu_sgdbe_SLOPESE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMASE3</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMASE3'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMASE3'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMASE2</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMASE2'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMASE2'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMASE1</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMASE1'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMASE1'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMADO3</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMADO3'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMADO3'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMADO2</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMADO2'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMADO2'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMADO1</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMADO1'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMADO1'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">WR</th>\
                <td>' + (feature.properties['smu_sgdbe_WR'] !== null ? autolinker.link(feature.properties['smu_sgdbe_WR'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">IL</th>\
                <td>' + (feature.properties['smu_sgdbe_IL'] !== null ? autolinker.link(feature.properties['smu_sgdbe_IL'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">ROO</th>\
                <td>' + (feature.properties['smu_sgdbe_ROO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_ROO'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">TXDEPCHG</th>\
                <td>' + (feature.properties['smu_sgdbe_TXDEPCHG'] !== null ? autolinker.link(feature.properties['smu_sgdbe_TXDEPCHG'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">TXSUBSE</th>\
                <td>' + (feature.properties['smu_sgdbe_TXSUBSE'] !== null ? autolinker.link(feature.properties['smu_sgdbe_TXSUBSE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">TXSUBDO</th>\
                <td>' + (feature.properties['smu_sgdbe_TXSUBDO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_TXSUBDO'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">TXSRFSE</th>\
                <td>' + (feature.properties['smu_sgdbe_TXSRFSE'] !== null ? autolinker.link(feature.properties['smu_sgdbe_TXSRFSE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">AGLIM2</th>\
                <td>' + (feature.properties['smu_sgdbe_AGLIM2'] !== null ? autolinker.link(feature.properties['smu_sgdbe_AGLIM2'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">AGLIM1</th>\
                <td>' + (feature.properties['smu_sgdbe_AGLIM1'] !== null ? autolinker.link(feature.properties['smu_sgdbe_AGLIM1'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">USESE</th>\
                <td>' + (feature.properties['smu_sgdbe_USESE'] !== null ? autolinker.link(feature.properties['smu_sgdbe_USESE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">USEDO</th>\
                <td>' + (feature.properties['smu_sgdbe_USEDO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_USEDO'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMASE</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMASE'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMASE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">PARMADO</th>\
                <td>' + (feature.properties['smu_sgdbe_PARMADO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_PARMADO'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">SLOPEDO</th>\
                <td>' + (feature.properties['smu_sgdbe_SLOPEDO'] !== null ? autolinker.link(feature.properties['smu_sgdbe_SLOPEDO'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_eu_soil_db_clipped_aragon__4_0(feature) {
    switch(String(feature.properties['nombre'])) {
        case 'Acrisol moliglósico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(255,248,44,1.0)',
        interactive: true,
    }
            break;
        case 'Calcisol arídico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(237,230,199,1.0)',
        interactive: true,
    }
            break;
        case 'Cambisol calcárico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(216,150,64,1.0)',
        interactive: true,
    }
            break;
        case 'Cambisol éutrico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(130,95,42,1.0)',
        interactive: true,
    }
            break;
        case 'Cambisol mólico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(214,91,30,1.0)',
        interactive: true,
    }
            break;
        case 'Fluvisol calcárico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(19,123,149,1.0)',
        interactive: true,
    }
            break;
        case 'Gipsisol arídico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(248,154,151,1.0)',
        interactive: true,
    }
            break;
        case 'Luvisol crómico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(210,14,110,1.0)',
        interactive: true,
    }
            break;
        case 'Planosol cálcárico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(237,93,110,1.0)',
        interactive: true,
    }
            break;
        case 'Planosol dístrico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(155,177,34,1.0)',
        interactive: true,
    }
            break;
        case 'Planosol háplico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(3,158,78,1.0)',
        interactive: true,
    }
            break;
        case 'Planosol réncico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(1,173,101,1.0)',
        interactive: true,
    }
            break;
        case 'Regosol éutrico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(237,228,127,1.0)',
        interactive: true,
    }
            break;
        case 'Solonchak háplico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(163,177,185,1.0)',
        interactive: true,
    }
            break;
        case 'Vertisol crómico':
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(242,77,164,1.0)',
        interactive: true,
    }
            break;
        default:
            return {
        pane: 'pane_eu_soil_db_clipped_aragon__4',
        opacity: 1,
        color: 'rgba(131,131,131,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(166,206,227,1.0)',
        interactive: true,
    }
            break;
    }
}
map.createPane('pane_eu_soil_db_clipped_aragon__4');
map.getPane('pane_eu_soil_db_clipped_aragon__4').style.zIndex = 404;
map.getPane('pane_eu_soil_db_clipped_aragon__4').style['mix-blend-mode'] = 'normal';
var layer_eu_soil_db_clipped_aragon__4 = new L.geoJson(json_eu_soil_db_clipped_aragon__4, {
    attribution: 'European Soil Database v2.0',
    interactive: true,
    dataVar: 'json_eu_soil_db_clipped_aragon__4',
    layerName: 'layer_eu_soil_db_clipped_aragon__4',
    pane: 'pane_eu_soil_db_clipped_aragon__4',
    onEachFeature: pop_eu_soil_db_clipped_aragon__4,
    style: style_eu_soil_db_clipped_aragon__4_0,
});
bounds_group.addLayer(layer_eu_soil_db_clipped_aragon__4);

//map.addLayer(layer_eu_soil_db_clipped_aragon__4);

map.createPane('pane_SabinaresalbaresJuniperusthurifera_5');
map.getPane('pane_SabinaresalbaresJuniperusthurifera_5').style.zIndex = 405;
var layer_SabinaresalbaresJuniperusthurifera_5 = L.WMS.layer("https://wms.mapama.gob.es/sig/Biodiversidad/MFE20/wms.aspx", "Sabinares albares (Juniperus thurifera)", {
    pane: 'pane_SabinaresalbaresJuniperusthurifera_5',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.createPane('pane_SabinaresdeJuniperusphoenicea_6');
map.getPane('pane_SabinaresdeJuniperusphoenicea_6').style.zIndex = 406;
var layer_SabinaresdeJuniperusphoenicea_6 = L.WMS.layer("https://wms.mapama.gob.es/sig/Biodiversidad/MFE6/wms.aspx", "Sabinares de Juniperus phoenicea", {
    pane: 'pane_SabinaresdeJuniperusphoenicea_6',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.createPane('pane_EncinaresQuercusilex_7');
map.getPane('pane_EncinaresQuercusilex_7').style.zIndex = 407;
var layer_EncinaresQuercusilex_7 = L.WMS.layer("http://wms.magrama.es/sig/Biodiversidad/MFE18/wms.aspx?", "Encinares (Quercus ilex)", {
    pane: 'pane_EncinaresQuercusilex_7',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.createPane('pane_RobledalesderoblepubescenteQuercushumilis_8');
map.getPane('pane_RobledalesderoblepubescenteQuercushumilis_8').style.zIndex = 408;
var layer_RobledalesderoblepubescenteQuercushumilis_8 = L.WMS.layer("https://wms.mapama.gob.es/sig/Biodiversidad/MFE14/wms.aspx", "Robledales de roble pubescente (Quercus humilis)", {
    pane: 'pane_RobledalesderoblepubescenteQuercushumilis_8',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.createPane('pane_RobledalesdeQuercusroburyoQuercuspetraea_9');
map.getPane('pane_RobledalesdeQuercusroburyoQuercuspetraea_9').style.zIndex = 409;
var layer_RobledalesdeQuercusroburyoQuercuspetraea_9 = L.WMS.layer("https://wms.mapama.gob.es/sig/Biodiversidad/MFE4/wms.aspx", "Robledales de Quercus robur y/o Quercus petraea", {
    pane: 'pane_RobledalesdeQuercusroburyoQuercuspetraea_9',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.createPane('pane_HayedosFagussylvatica_10');
map.getPane('pane_HayedosFagussylvatica_10').style.zIndex = 410;
var layer_HayedosFagussylvatica_10 = L.WMS.layer("http://wms.magrama.es/sig/Biodiversidad/MFE1/wms.aspx?", "Hayedos (Fagus sylvatica)", {
    pane: 'pane_HayedosFagussylvatica_10',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Banco de Datos de la Naturaleza (MITECO)',
});
map.addLayer(layer_HayedosFagussylvatica_10);
function pop_provincias_cnig_11(feature, layer) {
    var popupContent = '<table>\
            <tr>\
                <td colspan="2">' + (feature.properties['NATCODE'] !== null ? autolinker.link(feature.properties['NATCODE'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['NAMEUNIT'] !== null ? autolinker.link(feature.properties['NAMEUNIT'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['CODNUT1'] !== null ? autolinker.link(feature.properties['CODNUT1'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['CODNUT2'] !== null ? autolinker.link(feature.properties['CODNUT2'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['NUT2NAME'] !== null ? autolinker.link(feature.properties['NUT2NAME'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['LABEL1'] !== null ? autolinker.link(feature.properties['LABEL1'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_provincias_cnig_11_0() {
    return {
        pane: 'pane_provincias_cnig_11',
        opacity: 1,
        color: 'rgba(255,255,255,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1.0, 
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(243,166,178,0.0)',
        interactive: false,
    }
}
map.createPane('pane_provincias_cnig_11');
map.getPane('pane_provincias_cnig_11').style.zIndex = 411;
map.getPane('pane_provincias_cnig_11').style['mix-blend-mode'] = 'normal';
var layer_provincias_cnig_11 = new L.geoJson(json_provincias_cnig_11, {
    attribution: 'Límites administrativos (CNIG-IGN)',
    interactive: false,
    dataVar: 'json_provincias_cnig_11',
    layerName: 'layer_provincias_cnig_11',
    pane: 'pane_provincias_cnig_11',
    onEachFeature: pop_provincias_cnig_11,
    style: style_provincias_cnig_11_0,
});
bounds_group.addLayer(layer_provincias_cnig_11);
map.addLayer(layer_provincias_cnig_11);

var baseMaps = {
    "Base OSM Dark (CARTO)": layer_BaseOSMDarkCARTO_3,
    "Ortoimagen PNOA (IGN)": layer_OrtoimagenPNOAIGN_2,
    "Mapa topográfico (IGN)": layer_MapatopogrficoIGN_1,
    "OpenStreetMap": layer_OpenStreetMap_0,
};

var overlayMaps = {
    'Límites provinciales': layer_provincias_cnig_11,
    "Hayedos (Fagus sylvatica)": layer_HayedosFagussylvatica_10,
    "Robledales de Quercus robur y/o Quercus petraea": layer_RobledalesdeQuercusroburyoQuercuspetraea_9,
    "Robledales de roble pubescente (Quercus humilis)": layer_RobledalesderoblepubescenteQuercushumilis_8,
    "Encinares (Quercus ilex)": layer_EncinaresQuercusilex_7,
    "Sabinares de Juniperus phoenicea": layer_SabinaresdeJuniperusphoenicea_6,
    "Sabinares albares (Juniperus thurifera)": layer_SabinaresalbaresJuniperusthurifera_5,
    'Suelos (tipología):<br /><table><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Acrisolmoliglósico0.png" /></td><td>Acrisol moliglósico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Calcisolarídico1.png" /></td><td>Calcisol arídico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Cambisolcalcárico2.png" /></td><td>Cambisol calcárico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Cambisoléutrico3.png" /></td><td>Cambisol éutrico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Cambisolmólico4.png" /></td><td>Cambisol mólico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Fluvisolcalcárico5.png" /></td><td>Fluvisol calcárico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Gipsisolarídico6.png" /></td><td>Gipsisol arídico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Luvisolcrómico7.png" /></td><td>Luvisol crómico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Planosolcálcárico8.png" /></td><td>Planosol cálcárico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Planosoldístrico9.png" /></td><td>Planosol dístrico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Planosolháplico10.png" /></td><td>Planosol háplico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Planosolréncico11.png" /></td><td>Planosol réncico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Regosoléutrico12.png" /></td><td>Regosol éutrico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Solonchakháplico13.png" /></td><td>Solonchak háplico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_Vertisolcrómico14.png" /></td><td>Vertisol crómico</td></tr><tr><td style="text-align: center;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="legend/eu_soil_db_clipped_aragon__4_láminasdeagua15.png" /></td><td>Láminas de agua</td></tr></table>': layer_eu_soil_db_clipped_aragon__4,
}

L.control.layers(baseMaps,overlayMaps).addTo(map);

setBounds();
