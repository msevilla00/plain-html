// MAPA PARA MOSTRAR CAPAS RENURSE
// Creado por Miguel Sevilla-Callejo <msevilla[at]ipe.csic.es>
// 20230324

// Configuración para iluminar-seleccionar objetos

var highlightLayer;

function highlightFeature(e) {
    highlightLayer = e.target;

    if (e.target.feature.geometry.type === 'LineString') {
        highlightLayer.setStyle({
        color: '#ffff00',
        });
    } else {
        highlightLayer.setStyle({
        fillColor: '#ffff00',
        fillOpacity: 1
        });
    }
}

// Objeto mapa
var map = L.map('map', {
    zoomControl:true, maxZoom:18, minZoom:8
        }).fitBounds([[40.972515629635495,-4.082093479851194],[42.9770896087038,0.5899387798511949]]);

// añade coordenadas en la URL
var hash = new L.Hash(map);

// atribución (créditos) generales del mapa
map.attributionControl.setPrefix('Mapa creado desde <a href="https://qgis.org">QGIS</a> con plugin <a href="https://github.com/tomchadwin/qgis2web" target="_blank">qgis2web</a> usando <a href="https://leafletjs.com" title="A JS library for interactive maps">Leaflet</a> por M. Sevilla-Callejo CC By-SA 20230324');

var autolinker = new Autolinker({truncate: {length: 30, location: 'smart'}});

// Herramineta de medida

var measureControl = new L.Control.Measure({
    position: 'topleft',
    primaryLengthUnit: 'meters',
    secondaryLengthUnit: 'kilometers',
    primaryAreaUnit: 'sqmeters',
    secondaryAreaUnit: 'hectares'
});

measureControl.addTo(map);

document.getElementsByClassName('leaflet-control-measure-toggle')[0]
.innerHTML = '';
document.getElementsByClassName('leaflet-control-measure-toggle')[0]
.className += ' fas fa-ruler';

// bounds_group
var bounds_group = new L.featureGroup([]);

// Establecer límites del visor

//function setBounds() {} // sin límites

function setBounds() {
    map.setMaxBounds(map.getBounds());
}


// CAPAS -----------------------------------------------------

// Capa Stamen Terrain

map.createPane('pane_StamenTerrain_0');

map.getPane('pane_StamenTerrain_0').style.zIndex = 400;

var layer_StamenTerrain_0 = L.tileLayer('http://tile.stamen.com/terrain/{z}/{x}/{y}.png', {
    pane: 'pane_StamenTerrain_0',
    opacity: 1.0,
    attribution: 'Mapa base <a href="http://stamen.com">Stamen Design</a>, licencia <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>. Datos de <a href="http://openstreetmap.org">OpenStreetMap</a>, licencia <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
    minZoom: 8,
    maxZoom: 22,
    minNativeZoom: 0,
    maxNativeZoom: 20
});

layer_StamenTerrain_0;

//map.addLayer(layer_StamenTerrain_0);

// Mapa OpenStreetMap

map.createPane('pane_OpenStreetMap_1');

map.getPane('pane_OpenStreetMap_1').style.zIndex = 401;

var layer_OpenStreetMap_1 = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    pane: 'pane_OpenStreetMap_1',
    opacity: 1.0,
    attribution: 'Mapa base <a href="http://osm.org">OpenStreetMap</a>, licencia <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> y datos originales licencia <a href="http://www.openstreetmap.org/copyright">ODbL</a>.',
    minZoom: 8,
    maxZoom: 22,
    minNativeZoom: 0,
    maxNativeZoom: 19
});

layer_OpenStreetMap_1;

//map.addLayer(layer_OpenStreetMap_1);

// Cartografía raster del IGN

map.createPane('pane_MapatopogrficoIGN_2');

map.getPane('pane_MapatopogrficoIGN_2').style.zIndex = 402;

var layer_MapatopogrficoIGN_2 = L.WMS.layer("http://www.ign.es/wms-inspire/mapa-raster?", "mtn_rasterizado", {
    pane: 'pane_MapatopogrficoIGN_2',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Mapa base del <a href="http://ign.es">Int. Geográfico Nacional</a>',
});

// Mapa base (callejero) del IGN

map.createPane('pane_MapabaseIGN_3');

map.getPane('pane_MapabaseIGN_3').style.zIndex = 403;

var layer_MapabaseIGN_3 = L.WMS.layer("http://www.ign.es/wms-inspire/ign-base?", "IGNBaseTodo", {
    pane: 'pane_MapabaseIGN_3',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Mapa base del <a href="http://ign.es">Int. Geográfico Nacional</a>',
});

map.addLayer(layer_MapabaseIGN_3);

// Ortofoto PNOA reciente

map.createPane('pane_OrtofotoPNOA202021_4');

map.getPane('pane_OrtofotoPNOA202021_4').style.zIndex = 404;

var layer_OrtofotoPNOA202021_4 = L.WMS.layer("https://www.ign.es/wms-inspire/pnoa-ma", "OI.OrthoimageCoverage", {
    pane: 'pane_OrtofotoPNOA202021_4',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Ortofotografía del <a href="http://pnoa.ign.es">Plan Nacional de Ortofotografía Aérea (IGN)</a>',
});

// Ortofoto del vuelo americano

map.createPane('pane_OrtofotoVueloAmericano195657_5');

map.getPane('pane_OrtofotoVueloAmericano195657_5').style.zIndex = 405;

var layer_OrtofotoVueloAmericano195657_5 = L.WMS.layer("http://www.ign.es/wms/pnoa-historico?", "AMS_1956-1957", {
    pane: 'pane_OrtofotoVueloAmericano195657_5',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Ortofotografía del <a href="http://pnoa.ign.es">Plan Nacional de Ortofotografía Aérea (IGN)</a>',
});

// Capa SIOSE usos

map.createPane('pane_SIOSE2014Usosdesueloexistentes_6');

map.getPane('pane_SIOSE2014Usosdesueloexistentes_6').style.zIndex = 406;
var layer_SIOSE2014Usosdesueloexistentes_6 = L.WMS.layer("https://servicios.idee.es/wms-inspire/ocupacion-suelo", "LU.ExistingLandUse", {
    pane: 'pane_SIOSE2014Usosdesueloexistentes_6',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Mapa del <a href="http://siose.es">Sist. de Inf. sobre Ocupación del Suelo de España (IGN)</a>',
});

// Capa SIOSE superficies

map.createPane('pane_SIOSE2014Superficiesdecubiertaterrestre_7');

map.getPane('pane_SIOSE2014Superficiesdecubiertaterrestre_7').style.zIndex = 407;

var layer_SIOSE2014Superficiesdecubiertaterrestre_7 = L.WMS.layer("https://servicios.idee.es/wms-inspire/ocupacion-suelo", "LC.LandCoverSurfaces", {
    pane: 'pane_SIOSE2014Superficiesdecubiertaterrestre_7',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '© Mapa del <a href="http://siose.es">Sist. de Inf. sobre Ocupación del Suelo de España (IGN)</a>',
});

// Capa de cuencas de masas de agua

function pop_cuencasmasasdeagua_8(feature, layer) {
    layer.on({
        mouseout: function(e) {
            for (i in e.target._eventParents) {
                e.target._eventParents[i].resetStyle(e.target);
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2">' + (feature.properties['fid'] !== null ? autolinker.link(feature.properties['fid'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['cat'] !== null ? autolinker.link(feature.properties['cat'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_cuencasmasasdeagua_8_0() {
    return {
        pane: 'pane_cuencasmasasdeagua_8',
        opacity: 1,
        color: 'rgba(255,127,0,1.0)',
        dashArray: '',
        lineCap: 'square',
        lineJoin: 'bevel',
        weight: 1.0,
        fillOpacity: 0,
        interactive: false,
    }
}
map.createPane('pane_cuencasmasasdeagua_8');

map.getPane('pane_cuencasmasasdeagua_8').style.zIndex = 408;

map.getPane('pane_cuencasmasasdeagua_8').style['mix-blend-mode'] = 'normal';

var layer_cuencasmasasdeagua_8 = new L.geoJson(json_cuencasmasasdeagua_8, {
    attribution: '© Información masas de agua del <a href="https://www.miteco.gob.es/es/cartografia-y-sig/ide/descargas/agua/">MITECO</a>',
    interactive: false,
    dataVar: 'json_cuencasmasasdeagua_8',
    layerName: 'layer_cuencasmasasdeagua_8',
    pane: 'pane_cuencasmasasdeagua_8',
    onEachFeature: pop_cuencasmasasdeagua_8,
    style: style_cuencasmasasdeagua_8_0,
});

bounds_group.addLayer(layer_cuencasmasasdeagua_8);

// Capa de términos municipales

function pop_lmitesmunicipales_9(feature, layer) {
    layer.on({
        mouseout: function(e) {
            for (i in e.target._eventParents) {
                e.target._eventParents[i].resetStyle(e.target);
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2">' + (feature.properties['fid'] !== null ? autolinker.link(feature.properties['fid'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['cat'] !== null ? autolinker.link(feature.properties['cat'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_lmitesmunicipales_9_0() {
    return {
        pane: 'pane_lmitesmunicipales_9',
        opacity: 1,
        color: 'rgba(210,14,110,1.0)',
        dashArray: '',
        lineCap: 'square',
        lineJoin: 'bevel',
        weight: 1.0,
        fillOpacity: 0,
        interactive: false,
    }
}

map.createPane('pane_lmitesmunicipales_9');

map.getPane('pane_lmitesmunicipales_9').style.zIndex = 409;

map.getPane('pane_lmitesmunicipales_9').style['mix-blend-mode'] = 'normal';

var layer_lmitesmunicipales_9 = new L.geoJson(json_lmitesmunicipales_9, {
    attribution: '© Información de términos municipales del <a href="http://centrodedescargas.cnig.es">CNIG-IGN</a>',
    interactive: false,
    dataVar: 'json_lmitesmunicipales_9',
    layerName: 'layer_lmitesmunicipales_9',
    pane: 'pane_lmitesmunicipales_9',
    onEachFeature: pop_lmitesmunicipales_9,
    style: style_lmitesmunicipales_9_0,
});

bounds_group.addLayer(layer_lmitesmunicipales_9);

// Capa de buffer 5Km de las localidades

function pop_localidadesbuffer5km_10(feature, layer) {
    layer.on({
        mouseout: function(e) {
            for (i in e.target._eventParents) {
                e.target._eventParents[i].resetStyle(e.target);
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <td colspan="2">' + (feature.properties['id'] !== null ? autolinker.link(feature.properties['id'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['nombre'] !== null ? autolinker.link(feature.properties['nombre'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['lat'] !== null ? autolinker.link(feature.properties['lat'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['long'] !== null ? autolinker.link(feature.properties['long'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['tipo'] !== null ? autolinker.link(feature.properties['tipo'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['provincia'] !== null ? autolinker.link(feature.properties['provincia'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['municipio'] !== null ? autolinker.link(feature.properties['municipio'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <td colspan="2">' + (feature.properties['area'] !== null ? autolinker.link(feature.properties['area'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_localidadesbuffer5km_10_0() {
    return {
        pane: 'pane_localidadesbuffer5km_10',
        opacity: 1,
        color: 'rgba(255,255,255,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 4, 
        fill: true,
        fillOpacity: 0,
        fillColor: 'rgba(0,0,255,0.0)',
        interactive: false,
    }
}
function style_localidadesbuffer5km_10_1() {
    return {
        pane: 'pane_localidadesbuffer5km_10',
        opacity: 1,
        color: 'rgba(0,0,0,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 2.0, 
        fill: true,
        fillOpacity: 0,
        fillColor: 'rgba(227,26,28,0.0)',
        interactive: false,
    }
}
map.createPane('pane_localidadesbuffer5km_10');

map.getPane('pane_localidadesbuffer5km_10').style.zIndex = 410;

map.getPane('pane_localidadesbuffer5km_10').style['mix-blend-mode'] = 'normal';

var layer_localidadesbuffer5km_10 = new L.geoJson.multiStyle(json_localidadesbuffer5km_10, {
    attribution: '',
    interactive: false,
    dataVar: 'json_localidadesbuffer5km_10',
    layerName: 'layer_localidadesbuffer5km_10',
    pane: 'pane_localidadesbuffer5km_10',
    onEachFeature: pop_localidadesbuffer5km_10,
    styles: [style_localidadesbuffer5km_10_0,style_localidadesbuffer5km_10_1,]
});

bounds_group.addLayer(layer_localidadesbuffer5km_10);

map.addLayer(layer_localidadesbuffer5km_10);

// Puntos localidades

function pop_localidades_11(feature, layer) {
    layer.on({
        mouseout: function(e) {
            for (i in e.target._eventParents) {
                e.target._eventParents[i].resetStyle(e.target);
            }
        },
        mouseover: highlightFeature,
    });
    var popupContent = '<table>\
            <tr>\
                <th scope="row">id</th>\
                <td>' + (feature.properties['id'] !== null ? autolinker.link(feature.properties['id'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">nombre</th>\
                <td>' + (feature.properties['nombre'] !== null ? autolinker.link(feature.properties['nombre'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">lat</th>\
                <td>' + (feature.properties['lat'] !== null ? autolinker.link(feature.properties['lat'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">long</th>\
                <td>' + (feature.properties['long'] !== null ? autolinker.link(feature.properties['long'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">tipo</th>\
                <td>' + (feature.properties['tipo'] !== null ? autolinker.link(feature.properties['tipo'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">provincia</th>\
                <td>' + (feature.properties['provincia'] !== null ? autolinker.link(feature.properties['provincia'].toLocaleString()) : '') + '</td>\
            </tr>\
            <tr>\
                <th scope="row">municipio</th>\
                <td>' + (feature.properties['municipio'] !== null ? autolinker.link(feature.properties['municipio'].toLocaleString()) : '') + '</td>\
            </tr>\
        </table>';
    layer.bindPopup(popupContent, {maxHeight: 400});
}

function style_localidades_11_0() {
    return {
        pane: 'pane_localidades_11',
        radius: 6.0,
        opacity: 1,
        color: 'rgba(35,35,35,1.0)',
        dashArray: '',
        lineCap: 'butt',
        lineJoin: 'miter',
        weight: 1,
        fill: true,
        fillOpacity: 1,
        fillColor: 'rgba(255,127,0,1.0)',
        interactive: true,
    }
}

map.createPane('pane_localidades_11');

map.getPane('pane_localidades_11').style.zIndex = 411;

map.getPane('pane_localidades_11').style['mix-blend-mode'] = 'normal';

var layer_localidades_11 = new L.geoJson(json_localidades_11, {
    attribution: '',
    interactive: true,
    dataVar: 'json_localidades_11',
    layerName: 'layer_localidades_11',
    pane: 'pane_localidades_11',
    onEachFeature: pop_localidades_11,
    pointToLayer: function (feature, latlng) {
        var context = {
            feature: feature,
            variables: {}
        };
        return L.circleMarker(latlng, style_localidades_11_0(feature));
    },
});

bounds_group.addLayer(layer_localidades_11);

map.addLayer(layer_localidades_11);


// CAPAS AÑADIDAS --------------------------------------------------------------

// Mapa SIOSE AR + MDS LiDAR

map.createPane('pane_00RENURSESIOSEARLiDAR_0');

map.getPane('pane_00RENURSESIOSEARLiDAR_0').style.zIndex = 400;

var layer_00RENURSESIOSEARLiDAR_0 = L.tileLayer('https://valecam.csic.es/renurse/tms/siosear_lidar/{z}/{x}/{-y}.png', {
    pane: 'pane_00RENURSESIOSEARLiDAR_0',
    opacity: 1.0,
    attribution: 'Datos <a href="http://siose.es">Sist. de Inf. sobre Ocupación del Suelo de España</a> y <a href="http://pnoa.ign.org">MDS LiDAR</a> (IGN).',
    minZoom: 1,
    maxZoom: 28,
    minNativeZoom: 0,
    maxNativeZoom: 18
});

layer_00RENURSESIOSEARLiDAR_0;

//map.addLayer(layer_00RENURSESIOSEARLiDAR_0);


// Mapa de usos del suelo 80-90

map.createPane('pane_Mapadecultivos19801990150k_0');
map.getPane('pane_Mapadecultivos19801990150k_0').style.zIndex = 400;
var layer_Mapadecultivos19801990150k_0 = L.WMS.layer("https://wms.mapama.gob.es/sig/Agricultura/MapaCultivos1980-1990/wms.aspx", "LC.LandCoverSurfaces", {
    pane: 'pane_Mapadecultivos19801990150k_0',
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: 'Minist. Agricultura, Pesca y Alimentación',
});

layer_Mapadecultivos19801990150k_0;

//map.addLayer(layer_Mapadecultivos19801990150k_0);

// GEOLOCALIZAR ---------------------------------------------------------------
// https://github.com/domoritz/leaflet-locatecontrol

L.control.locate(
    {strings: {
        title: "Muéstrame mi localización"
      }}
).addTo(map);


// ESCALA ---------------------------------------------------------------------

L.control.scale(
    {position: 'topleft',
    maxWidth: 100, 
    metric: true,
    imperial: false,
    updateWhenIdle: false
}).addTo(map);


// MOSTRAR COORDENADAS DE UN PUNTO --------------------------------------------
var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("Coordenadas de este punto: " + e.latlng.toString())
        .openOn(map);
}

map.on('click', onMapClick);



// LÍMITES ¿? -----------------------------------------------------------------
setBounds();


// VENTANA EMERGENTE ----------------------------------------------------------

var winOpts = L.control.window(map,{
    title:'<p style="text-align:center">Visor cartográfico del proyecto RENURSE</p>',
    content:'<p style="text-align:center"><img src="https://riosciudadanos.csic.es/wp-content/uploads/2022/04/IPE_logo_horizontal_color.svg_.750px.png" title="#RiosCiudadanos" width="300"></p><p style="text-align:center">A través de este mapa interactivo puedes ver información espacial relevante para el proyecto RENURSE:  <i>Efecto de la <b>RE</b>stauración de <b>NÚ</b>cleos <b>R</b>urales abandonados sobre los <b>S</b>ervicios <b>E</b>cosistémicos: hacia una transición ecológica sostenible del medio rural</i>.</p><p style="text-align:center">Miguel Sevilla-Callejo - msevilla@ipe.csic.es<br>25 de abril de 2023</p>',
    visible: true});

// MINIMAP --------------------------------------------------------------------

// minimap con OSM
var layer_OpenStreetMap_2b = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    opacity: 1.0,
    attribution: '',
    minZoom: 1,
    maxZoom: 19,
    minNativeZoom: 0,
    maxNativeZoom: 19
});
layer_OpenStreetMap_2b;


var miniMap = new L.Control.MiniMap(
    layer_OpenStreetMap_2b,
    {toggleDisplay: true,
    position:'bottomright',
    zoomLevelOffset:-5
    })



/*/ Minimap con capa de Stamen
var layer_StamenTerrain_0b = L.tileLayer('http://tile.stamen.com/terrain/{z}/{x}/{y}.png', {
    opacity: 1.0,
    attribution: '',
    minZoom: 1,
    maxZoom: 19,
    minNativeZoom: 0,
    maxNativeZoom: 19
});

layer_StamenTerrain_0b;

var miniMap = new L.Control.MiniMap(
    layer_StamenTerrain_0b,
    {toggleDisplay: true,
    position:'bottomright',
    zoomLevelOffset:-5
    })
*/

/* pruebas con base IGN

var layer_MapabaseIGN_3b = L.WMS.layer("http://www.ign.es/wms-inspire/ign-base?", "IGNBaseTodo", {
    format: 'image/png',
    uppercase: true,
    transparent: true,
    continuousWorld : true,
    tiled: true,
    info_format: 'text/html',
    opacity: 1,
    identify: false,
    attribution: '',
});

layer_MapabaseIGN_3b;

var miniMap = new L.Control.MiniMap(
    layer_MapabaseIGN_3b,
    {toggleDisplay: true,
    position:'bottomright',
    zoomLevelOffset:-5
    })


*/

miniMap.addTo(map);


// BOTONES LOCALIZACIÓN -------------------------------------------------------

// Define el control personalizado de zoom y posición
L.Control.zoomPosition = L.Control.extend({
    options: {
      position: 'topright'
    },
  
    onAdd: function(map) {
      var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
  
        // Crea el botón de Guadalajara - 12/41.0539/-3.3271
        var nyButton = L.DomUtil.create('button', 'my-zoom-button', container);
        nyButton.title = 'Guadalajara';
        nyButton.innerHTML = 'GU';
        L.DomEvent.addListener(nyButton, 'click', function() {
            map.setView([41.0539, -3.3271], 12);
        }, this);
    
        // Crea el botón de Huesca - 12/42.3911/-0.2867
        var nyButton = L.DomUtil.create('button', 'my-zoom-button', container);
        nyButton.title = 'Huesca';
        nyButton.innerHTML = 'HU';
        L.DomEvent.addListener(nyButton, 'click', function() {
            map.setView([42.3911, -0.2867], 12);
        }, this);

        // Crea el botón de Navarra - 12/42.8906/-1.3733
        var sfButton = L.DomUtil.create('button', 'my-zoom-button', container);
        sfButton.title = 'Navarra';
        sfButton.innerHTML = 'NA';
        L.DomEvent.addListener(sfButton, 'click', function() {
            map.setView([42.8906, -1.3733], 12);
        }, this);
  
        // Crea el botón de restablecer la vista original - 8/41.983/-1.746
        var resetButton = L.DomUtil.create('button', 'my-zoom-button', container);
        resetButton.title = 'Restablecer vista original';
        resetButton.innerHTML = 'Reset';
        L.DomEvent.addListener(resetButton, 'click', function() {
            map.setView([41.983, -1.746],8);
        }, this);
  
      return container;
    }
  });
  
  
  // Añade el control personalizado al mapa
  L.control.zoomPosition = function(opts) {
    return new L.Control.zoomPosition(opts);
  }
  
  L.control.zoomPosition().addTo(map);


  // CONTROL DE CAPAS -----------------------------------------------------------

    // capas base
    var baseMaps = {
        "Mapa de usos y aprovechamientos 1980-1990":layer_Mapadecultivos19801990150k_0,
        "SIOSE 2014 - Superficies de cubierta terrestre": layer_SIOSE2014Superficiesdecubiertaterrestre_7,
        "SIOSE 2014 - Usos de suelo existentes": layer_SIOSE2014Usosdesueloexistentes_6,
        "SIOSE AR 2017 + MDS LiDAR": layer_00RENURSESIOSEARLiDAR_0,
        "Ortofoto - Vuelo Americano 1956/57)": layer_OrtofotoVueloAmericano195657_5,
        "Ortofoto - PNOA 2020/21": layer_OrtofotoPNOA202021_4,
        "Mapa base IGN": layer_MapabaseIGN_3,
        "Mapa topográfico IGN": layer_MapatopogrficoIGN_2,
        "OpenStreetMap": layer_OpenStreetMap_1,
        "Stamen Terrain": layer_StamenTerrain_0,
    };

    // capas superpuestas
    var overlayMaps = {
        '<img src="legend/localidades_11.png" /> localidades': layer_localidades_11,
        '<img src="legend/localidadesbuffer5km_10.png" /> localidades - buffer 5km': layer_localidadesbuffer5km_10,
        '<img src="legend/lmitesmunicipales_9.png" /> límites municipales': layer_lmitesmunicipales_9,
        '<img src="legend/cuencasmasasdeagua_8.png" /> cuencas masas de agua': layer_cuencasmasasdeagua_8,
    };

    // añadir control
    L.control.layers(
        baseMaps,
        overlayMaps,
        {collapsed:true}
        ).addTo(map);